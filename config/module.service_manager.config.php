<?php
return [
    'abstract_factories' => [
        \Zend\Cache\Service\StorageCacheAbstractServiceFactory::class,
        \Zend\Log\LoggerAbstractServiceFactory::class,
    ],
    'factories' => [
        'application.portAdapter.persistence.pdo.DomainSession' => \Application\PortAdapter\Persistence\PDO\DomainSessionFactory::class,

        'application.portAdapter.persistence.lib.EntityManager' => \Application\PortAdapter\Persistence\lib\EntityManagerFactory::class,

        'application.application.testObject.TestObjectService' => \Application\Application\TestObject\TestObjectServiceFactory::class,

        'application.portAdapter.testObject.persistence.TestObjectRepository' => \Application\PortAdapter\TestObject\Persistence\PDO\TestObjectRepositoryFactory::class,
    ]
];
