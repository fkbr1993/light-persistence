<?php
return [
    'routes' => [
        'api' => [
            'type' => 'Literal',
            'options' => [
                'route' => '/api',
                'defaults' => [
                    'controller' => 'Index',
                    'action' => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes' => [
                'testObject' => [
                    'type' => 'Segment',
                    'options' => [
                        'route' => '/testObject[/:action]',
                        'constraints' => [
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ],
                        'defaults' => [
                            'action' => 'index',
                            'controller' => 'TestObject',
                        ],
                    ],
                ],
            ],
        ],
    ],
];
