<?php
return array(
    'router' => require dirname(__FILE__) . '/module.router.config.php',
    'service_manager' =>  require dirname(__FILE__) . '/module.service_manager.config.php',
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'TestObject' => \Application\PortAdapter\TestObject\API\Http\Controller\TestObjectController::class,
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404'     => __DIR__ . '/../view/error/404.phtml',
            'error/index'   => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],
        'strategies'               => [
            'ViewJsonStrategy'
        ],
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(

            ),
        ),
    ),
    'memcached'          => [
        'host' => 'localhost',
        'port' => 11211
    ],
    'db' => [
        'connection' => [
            'platform' => 'pgsql',
            'host'     => 'localhost',
            'port'     => '5432',
            'user'     => 'persistence',
            'password' => '123456',
            'dbname'   => 'report',
            'charset'  => 'utf8',
        ],
    ],

);
