<?php
namespace Application\Application\TestObject;

use Application\Application\OperationResponse;
use Application\Domain\DomainSessionInterface;
use Application\Domain\PersistModel\TestObject\TestObjectFactory;
use Application\Domain\PersistModel\TestObject\TestObjectRepositoryInterface;

/**
 * Class TestObjectService
 * @package Application\Application\TestObjectService
 */
class TestObjectService
{
    /**
     * @var TestObjectRepositoryInterface
     */
    private $testObjectRepository;

    /**
     * @var DomainSessionInterface
     */
    private $domainSession;

    /**
     * @var TestObjectFactory
     */
    protected $testObjectFactory;

    /**
     * RoleService constructor.
     * @param DomainSessionInterface $domainSession
     * @param TestObjectRepositoryInterface $testObjectRepository
     * @param TestObjectFactory $testObjectFactory
     */
    public function __construct(
        DomainSessionInterface $domainSession,
        TestObjectRepositoryInterface $testObjectRepository,
        TestObjectFactory $testObjectFactory
    )
    {
        $this->domainSession      = $domainSession;
        $this->testObjectRepository = $testObjectRepository;
        $this->testObjectFactory = $testObjectFactory;
    }

    /**
     * @param string $name
     * @param integer $state
     *
     * @return OperationResponse
     */
    public function create($name, $state)
    {
        $success = null;
        $data = null;
        $message = null;

        $testObject = $this->testObjectFactory->create($name, $state);

        try {
            $this->testObjectRepository->save($testObject);
            $this->domainSession->flush();
            $data = $testObject->getId();
            $success = true;
        } catch (\Exception $e ){
            $success = false;
            $message = $e->getMessage();
        }

        $response = new OperationResponse($success, $data, $message);

        return $response;
    }

    /**
     * @param integer $id
     * @param array $updateData
     * @return OperationResponse
     */
    public function update($id, array $updateData)
    {
        $success = null;
        $data = null;
        $message = null;


        try {
            $testObject = $this->testObjectRepository->find($id);

            $testObject->update($updateData);

            $this->testObjectRepository->save($testObject);
            $this->domainSession->flush();

            $data = $testObject->getId();
            $success = true;
        } catch (\Exception $e ){
            $success = false;
            $message = $e->getMessage();
        }

        $response = new OperationResponse($success, $data, $message);

        return $response;
    }

    /**
     * @param $id
     * @return OperationResponse
     */
    public function delete($id)
    {
        $success = null;
        $data = null;
        $message = null;

        $testObject = $this->testObjectRepository->find($id);

        try {
            $this->testObjectRepository->delete($testObject);
            $this->domainSession->flush();
            $data = $testObject->getId();
            $success = true;
        } catch (\Exception $e ){
            $success = false;
            $message = $e->getMessage();
        }

        $response = new OperationResponse($success, $data, $message);

        return $response;
    }

    public function find()
    {

    }
}
