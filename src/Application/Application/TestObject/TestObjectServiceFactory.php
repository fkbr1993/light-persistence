<?php
namespace Application\Application\TestObject;

use Application\Domain\DomainSessionInterface;
use Application\Domain\PersistModel\TestObject\TestObjectFactory;
use Application\Domain\PersistModel\TestObject\TestObjectRepositoryInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class TestObjectServiceFactory
 * @package Application\Application\TestObject
 */
class TestObjectServiceFactory implements FactoryInterface
{

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return TestObjectService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var DomainSessionInterface $domainSession */
        $domainSession = $serviceLocator->get('application.portAdapter.persistence.pdo.DomainSession');

        /** @var TestObjectRepositoryInterface $entityRepository */
        $entityRepository = $serviceLocator->get('application.portAdapter.testObject.persistence.TestObjectRepository');

        $testObjectFactory = new TestObjectFactory();

        return new TestObjectService($domainSession, $entityRepository, $testObjectFactory);
    }
}
