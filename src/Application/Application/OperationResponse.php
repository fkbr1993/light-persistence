<?php
namespace Application\Application;

/**
 * Class OperationResponse
 * @package Application\Application
 */
class OperationResponse implements \JsonSerializable
{
    /**
     * @var mixed
     */
    protected $data;

    /**
     * @var boolean
     */
    public $success;

    /**
     * @var string - error message on false
     */
    public $message;

    /**
     * @param $success
     * @param $data
     * @param null $message
     */
    public function __construct($success, $data, $message = null)
    {
        $this->success = $success;
        $this->data    = $data;
        $this->message = $message;
    }

    /**
     * @return mixed|string
     */
    public function jsonSerialize()
    {
        return [
            'data'    => $this->data,
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
