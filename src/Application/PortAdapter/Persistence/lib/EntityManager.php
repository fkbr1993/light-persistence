<?php
namespace Application\PortAdapter\Persistence\lib;

/**
 * Class EntityManager
 */
class EntityManager implements EntityManagerInterface
{

    /**
     * @var Connection $connection
     */
    private $connection;

    /**
     * @var UnitOfWOrk $unitOfWork
     */
    private $unitOfWork;

    /**
     * @var array $classMetaData
     */
    private $classMetaDataList;

    /**
     * @var EntityRepositoryFactory $repositoryFactory
     */
    private $repositoryFactory;

    /**
     * @param Connection $connection
     * @param array $classMetaDataList
     * @param EntityRepositoryFactory $repositoryFactory
     */
    public function __construct(Connection $connection, $classMetaDataList, EntityRepositoryFactory $repositoryFactory)
    {
        $this->connection = $connection;
        $this->unitOfWork = new UnitOfWork($this);
        $this->classMetaDataList = $classMetaDataList;
        $this->repositoryFactory = $repositoryFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return UnitOfWOrk
     */
    public function getUnitOfWork(){
        return $this->unitOfWork;
    }

    /**
     * Flushes all changes to objects that have been queued up to now to the database.
     * This effectively synchronizes the in-memory state of managed objects with the
     * database.
     *
     * If an entity is explicitly passed to this method only this entity and
     * the cascade-persist semantics + scheduled inserts/removals are synchronized.
     *
     * @return void
     */
    public function flush()
    {
        $this->unitOfWork->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        if (!is_object($entity)){
            throw PersistenceException::invalidObject('EntityManager#persist', $entity);
        }

        $this->unitOfWork->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function delete($entity)
    {
        if (!is_object($entity)){
            throw PersistenceException::invalidObject('EntityManager#delete', $entity);
        }

        $this->unitOfWork->delete($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityClassName, $id)
    {
        return $this->unitOfWork->load($entityClassName, $id);
    }

    /**
     * @param string $entityClassName
     *
     * @return ClassMetaData
     * @throws PersistenceException
     */
    public function getClassMetaData($entityClassName)
    {
        if (!empty($this->classMetaDataList['application']['entities'][$entityClassName])){
            return new ClassMetaData($this->classMetaDataList['application']['entities'][$entityClassName], $entityClassName);
        }
        throw new PersistenceException('Class ' . $entityClassName . ' not registered as an entity.');
    }

    /**
     * @param string $entityClassName
     *
     * @return EntityRepositoryInterface
     */
    public function getRepository($entityClassName){
        return $this->repositoryFactory->getRepository($this, $entityClassName);
    }
}
