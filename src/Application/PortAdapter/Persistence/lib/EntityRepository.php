<?php
namespace Application\PortAdapter\Persistence\lib;

/**
 * Class EntityRepository
 */
class EntityRepository implements EntityRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ClassMetaData
     */
    protected $classMetaData;

    /**
     * Initializes a new <tt>EntityRepository</tt>.
     *
     * @param EntityManagerInterface $entityManager The EntityManager to use.
     * @param ClassMetadata $classMetaData The class descriptor.
     */
    public function __construct(EntityManagerInterface $entityManager, ClassMetadata $classMetaData)
    {
        $this->entityManager = $entityManager;
        $this->classMetaData = $classMetaData;
    }

    /**
     * Finds an entity by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function find($id)
    {
        return $this->entityManager->find($this->getEntityClassName(), $id);
    }

    /**
     * Finds all entities in the repository.
     *
     * @return array The entities.
     */
    public function findAll()
    {
        return $this->findBy([]);
    }

    /**
     * Finds entities by a set of criteria.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     *
     * @return array The objects.
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $persister = $this->entityManager->getUnitOfWork()->getEntityPersister($this->getEntityClassName());

        return $persister->loadAll($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        $persister = $this->entityManager->getUnitOfWork()->getEntityPersister($this->getEntityClassName());

        return $persister->load($criteria, null, null, array(), null, 1, $orderBy);
    }

    /**
     * @return string
     */
    public function getEntityClassName()
    {
        return $this->getClassMetadata()->getEntityClassName();
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return ClassMetadata
     */
    protected function getClassMetadata()
    {
        return $this->classMetaData;
    }
}
