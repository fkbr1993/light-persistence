<?php
namespace Application\PortAdapter\Persistence\lib;

/**
 * Interface for entity repository factory.
 */
interface RepositoryFactoryInterface
{
    /**
     * Gets the repository for an entity class.
     *
     * @param EntityManagerInterface $entityManager The EntityManager instance.
     * @param string $entityClassName The the entity instance class name.
     *
     * @return EntityRepositoryInterface
     */
    public function getRepository(EntityManagerInterface $entityManager, $entityClassName);
}
