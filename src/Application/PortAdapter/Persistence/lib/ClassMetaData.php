<?php
namespace Application\PortAdapter\Persistence\lib;

/**
 * Class ClassMetaData
 * @package Application\PortAdapter\Persistence\lib
 */
class ClassMetaData
{
    private $className;
    private $entityClassName;
    private $tableName;
    private $properties;

    /**
     * @param array $entityConfig
     * @param string $entityClassName
     *
     * @throws PersistenceException
     */
    public function __construct( array $entityConfig, $entityClassName )
    {
        if (empty($entityConfig['className'])){
            throw new PersistenceException('ClassName must be set for entity configuration.');
        } else {
            $this->className = $entityConfig['className'];
        }

        if (empty($entityConfig['tableName'])){
            throw new PersistenceException('TableName must be set for entity configuration.');
        } else {
            $this->tableName = $entityConfig['tableName'];
        }
        if (empty($entityConfig['properties'])){
            throw new PersistenceException('Properties must be set for entity configuration.');
        } else {
            $this->properties = $entityConfig['properties'];
        }

        $this->entityClassName = $entityClassName;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @return string
     */
    public function getEntityClassName()
    {
        return $this->entityClassName;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @throws PersistenceException
     * @return string
     */
    public function getPrimaryKeyColumnName(){
        $properties = $this->getProperties();

        foreach($properties as $columnName => $columnData){
            if (isset($columnData['option']) && $columnData['option'] == 'primary key') {
                $primaryKeyColumnName = $columnName;
                break;
            }
        }
        if (empty($primaryKeyColumnName)){
            throw new PersistenceException('Entity class config primary key must be set.');
        }
        return $primaryKeyColumnName;
    }
}
