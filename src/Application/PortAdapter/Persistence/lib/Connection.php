<?php
namespace Application\PortAdapter\Persistence\lib;

/**
 * Class Connection
 * @package Application\PortAdapter\Persistence\lib
 */
class Connection
{

    /**
     * @var bool
     */
    private $isConnected = false;

    /**
     * @var array
     */
    private $config = [];

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return bool|\PDO
     */
    public function connect()
    {
        if ($this->isConnected) return false;

        $username = isset($this->config['user']) ? $this->config['user'] : '';
        $password = isset($this->config['password']) ? $this->config['password'] : '';
        $dbname = isset($this->config['dbname']) ? $this->config['dbname'] : '';
        $platform = isset($this->config['platform']) ? $this->config['platform'] : 'pgsql';
        $port = isset($this->config['port']) ? $this->config['port'] : '';
        $host = isset($this->config['host']) ? $this->config['host'] : '';
        $options = isset($this->config['options']) ? $this->config['options'] : [];

        $dsn = $platform.':dbname=' . $dbname.';host='.$host.';port='.$port;

        try {
            $pdo = new \PDO($dsn, $username, $password, $options);
            return $pdo;
        } catch (\PDOException $e){
            throw new $e('Cannot connect to database.');
        }
    }

    /**
     * @return string|null
     */
    public function getPlatform(){
        return $this->config['platform'];
    }

}
