<?php


namespace Application\PortAdapter\Persistence\lib;

/**
 * Class PersistenceException
 *
 * @package Application\PortAdapter\Persistence\lib
 */
class PersistenceException extends \Exception
{
    /**
     * @param string $context
     * @param mixed  $given
     * @param int    $parameterIndex
     *
     * @return PersistenceException
     */
    public static function invalidObject($context, $given, $parameterIndex = 1)
    {
        return new self($context . ' expects parameter ' . $parameterIndex .
            ' to be an entity object, '. gettype($given) . ' given.');
    }
}
