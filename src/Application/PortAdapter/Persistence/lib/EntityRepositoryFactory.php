<?php
namespace Application\PortAdapter\Persistence\lib;

/**
 * This factory is used to create default repository objects for entities at runtime.
 *
 */
final class EntityRepositoryFactory implements RepositoryFactoryInterface
{
    /**
     * The list of EntityRepository instances.
     *
     * @var EntityRepositoryInterface[]
     */
    private $repositoryList = [];

    /**
     * {@inheritdoc}
     */
    public function getRepository(EntityManagerInterface $entityManager, $entityClassName)
    {
        $repositoryHash = $entityClassName . spl_object_hash($entityManager);

        if (isset($this->repositoryList[$repositoryHash])) {
            return $this->repositoryList[$repositoryHash];
        }

        return $this->repositoryList[$repositoryHash] = $this->createRepository($entityManager, $entityClassName);
    }

    /**
     * Create a new repository instance for an entity class.
     *
     * @param EntityManagerInterface $entityManager The EntityManager instance.
     * @param string $entityClassName The the entity instance class name.
     *
     * @return EntityRepositoryInterface
     */
    private function createRepository(EntityManagerInterface $entityManager, $entityClassName)
    {
        $classMetaData = $entityManager->getClassMetaData($entityClassName);

        return new EntityRepository($entityManager, $classMetaData);
    }
}
