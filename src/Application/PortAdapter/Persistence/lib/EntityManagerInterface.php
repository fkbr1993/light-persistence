<?php
namespace Application\PortAdapter\Persistence\lib;

/**
 * Interface EntityManagerInterface
 *
 * @package Application\PortAdapter\Persistence\lib
 */
Interface EntityManagerInterface{
    /**
     * Returns database connection object
     * @return Connection
     */
    public function getConnection();

    /**
     * Flushes all changes to objects that have been queued up to now to the database.
     * This effectively synchronizes the in-memory state of managed objects with the
     * database.
     *
     * @return void
     */
    public function flush();

    /**
     * Tells the ObjectManager to make an instance managed and persistent.
     *
     * The object will be entered into the database as a result of the flush operation.
     *
     * @param object $object The instance to make managed and persistent.
     *
     * @return void
     */
    public function persist($object);

    /**
     * Tells the ObjectManager to set instance removed and cancel all the operations with it.
     *
     * The object will be deleted from the database as a result of the flush operation.
     *
     * @param object $object The instance to delete.
     *
     * @return void
     */
    public function delete($object);

    /**
     * Finds an object by its identifier.
     *
     * @param string $className The class name of the object to find.
     * @param mixed $id The identity of the object to find.
     *
     * @return object The found object.
     */
    public function find($className, $id);

    /**
     * @param string $entityClassName the instance class name to get metadata about
     *
     * @return ClassMetaData
     */
    public function  getClassMetaData($entityClassName);

    /**
     * @return UnitOfWork
     */
    public function getUnitOfWork();

    /**
     * @param string $entityClass the instance to get related repository
     * @return EntityRepositoryInterface
     */
    public function getRepository($entityClass);
}
