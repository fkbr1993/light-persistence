<?php
namespace Application\PortAdapter\Persistence\lib;

use Aura\SqlQuery\QueryFactory;

/**
 * Class UnitOfWork
 *
 * The UnitOfWork is responsible for tracking changes to objects during an
 * "object-level" transaction and for writing out changes to the database
 * in the correct order.
 */
class UnitOfWork
{

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * The states of any known entities.
     * Keys are object ids (spl_object_hash).
     * @var array
     */
    private $entityStates  = [];

    /**
     * A list of all pending entity deletions.
     * @var array
     */
    private $entityDeletions  = [];

    /**
     * A list of all pending entity updates.
     * @var array
     */
    private $entityUpdates = [];

    /**
     * A list of all pending entity insertions.
     * @var array
     */
    private $entityInsertions  = [];

    /**
     * The identity map that holds references to all managed entities that have
     * an identity. The entities are grouped by their class name and primary key.
     *
     * @var array
     */
    private $identityMap = [];

    /**
     * An entity is new if it has just been instantiated (i.e. using the "new" operator)
     * practically this means that entity has no primary key property set.
     */
    const ENTITY_STATE_NEW = 1;

    /**
     * An entity is in MANAGED state when its persistence is managed by an EntityManager.
     */
    const ENTITY_STATE_MANAGED = 2;

    /**
     * A removed entity instance is an entity instance removed both from EntityManager,
     * and database
     */
   const ENTITY_STATE_REMOVED = 3;


    /**
     * Initializes a new UnitOfWork instance, bound to the given EntityManager.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Returns EntityManager which is associated with this UnitOfWork
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Persists an entity as part of the current unit of work.
     *
     * @param object $entity The entity to persist.
     *
     * @return void
     */
    public function persist($entity)
    {
        $state = $this->getEntityState($entity);

        if ($state == self::ENTITY_STATE_NEW){
            $this->persistNew($entity);
        } elseif ($state == self::ENTITY_STATE_MANAGED) {
            $this->updateDirty($entity);
        } elseif ($state == self::ENTITY_STATE_REMOVED){
            // Entity becomes managed again
            $oid = spl_object_hash($entity);
            unset($this->entityDeletions[$oid]);
            $this->entityStates[$oid] = self::ENTITY_STATE_MANAGED;
        }
    }

    /**
     * Returns an entity instance state
     * @param $entity
     *
     * @return int
     */
    public function getEntityState($entity)
    {
        $oid = spl_object_hash($entity);
        if (isset($this->entityStates[$oid])){
            return $this->entityStates[$oid];
        }
        if (empty($entity->id)){
            return $this->entityStates[$oid] = self::ENTITY_STATE_NEW;
        } else {
            return $this->entityStates[$oid] = self::ENTITY_STATE_MANAGED;
        }
    }

    /**
     * Adds an entity instance to insertion schedule and assigns new state
     *
     * @param $entity
     *
     * @throws PersistenceException
     */
    public function persistNew($entity)
    {
        $oid = spl_object_hash($entity);

        if (isset($this->entityInsertions[$oid])) {
            throw new PersistenceException("Entity scheduled for insert twice.");
        }

        $this->entityInsertions[$oid] = $entity;

        $this->entityStates[$oid] = self::ENTITY_STATE_NEW;
    }

    /**
     * Adds an entity instance to update schedule
     *
     * @param $entity
     *
     * @throws PersistenceException
     */
    public function updateDirty($entity)
    {
        $oid = spl_object_hash($entity);

        if ($this->getEntityState($entity) == self::ENTITY_STATE_REMOVED) {
            throw new PersistenceException("Removed entity cannot be scheduled for update.");
        }

        if ($this->getEntityState($entity) == self::ENTITY_STATE_NEW) {
            throw new PersistenceException("Not inserted entity cannot be scheduled for update.");
        }

        $this->entityUpdates[$oid] = $entity;
    }

    /**
     * Adds an entity instance to delete schedule.
     *
     * @param object $entity The entity to delete.
     *
     * @return void
     */
    public function delete($entity)
    {
        $state = $this->getEntityState($entity);
        $oid = spl_object_hash($entity);

        // clear any schedule data
        if ($state == self::ENTITY_STATE_NEW){
            unset($this->entityInsertions[$oid]);
        } elseif ($state == self::ENTITY_STATE_MANAGED) {
            unset($this->entityUpdates[$oid]);
        }

        $this->entityDeletions[$oid] = $entity;
    }

    /**
     * Commits the UnitOfWork, executing all operations that have been postponed
     * up to this point.
     *
     * @throws PersistenceException
     */
    public function commit()
    {
        if ( !($this->entityInsertions || $this->entityDeletions || $this->entityUpdates ) ){
            return; // Nothing to do.
        }

        $pdo = $this->getEntityManager()->getConnection()->connect();
        $pdo->beginTransaction();

        try {
            foreach ($this->entityInsertions as $insert) {
                $this->executeInsert($insert, $pdo);
            }

            foreach ($this->entityUpdates as $update) {
                $this->executeUpdate($update, $pdo);
            }

            foreach ($this->entityDeletions as $delete) {
                $this->executeDelete($delete, $pdo);
            }

            $pdo->commit();
        } catch (PersistenceException $e) {
            $pdo->rollback();
            throw $e;
        }

        // Clear up
        $this->entityInsertions =
        $this->entityUpdates =
        $this->entityDeletions = [];
    }

    /**
     * @param object $object The entity instance
     * @param \PDO $pdo
     * @throws PersistenceException
     */
    public function executeInsert($object, \PDO $pdo)
    {
        $classMetaData = $this->getClassMetaData($object);
        $tableName = $classMetaData->getTableName();
        $properties = $classMetaData->getProperties();

        $columns =
        $values = [];
        $primaryKeyColumnName = '';
        
        foreach($properties as $columnName => $columnData){
            //TODO: implement with public getters or reflection???
            //way 1: with public getters
            $getterFunc = 'get'. ucfirst($columnName);

            if (isset($columnData['option']) && $columnData['option'] == 'primary key'){
                $primaryKeyColumnName = $columnName;
            }

            if (method_exists($object, $getterFunc) && !empty($object->$getterFunc())){
                $columns[] = $columnName;
                if ($columnData['type'] != 'integer'){
                    $values[] = "'" . $object->$getterFunc() . "'";
                } else {
                    $values[] = $object->$getterFunc();
                }
            }
        }

        $columns = implode(',', $columns);
        $values = implode(',', $values);

        $sql = sprintf('INSERT INTO "%s" (%s) VALUES (%s)', $tableName, $columns, $values);

        $sth = $pdo->prepare($sql);

        if (!$sth->execute()){
            throw new PersistenceException('Cannot insert model to database: ' . $pdo->errorInfo()[2]);
        }

        $oid = spl_object_hash($object);
        $this->entityStates[$oid] = self::ENTITY_STATE_MANAGED;

        $seqName = $tableName . '_' . $primaryKeyColumnName . '_seq';
        $primaryKeyValue = $pdo->lastInsertId($seqName);

        //way 2: with reflection
        $refObject   = new \ReflectionObject( $object );
        $refProperty = $refObject->getProperty( 'id' );
        if (!$refProperty->isPublic()){
            $refProperty->setAccessible( true );
        }
        $refProperty->setValue($object, $primaryKeyValue );
        if (!$refProperty->isPublic()){
            $refProperty->setAccessible( false );
        }

        $this->addToIdentityMap($object);
    }

    /**
     * Executes all scheduled entity updates
     *
     * @param object $object The entity instance
     * @param \PDO $pdo
     *
     * @throws PersistenceException
     */
    public function executeUpdate($object, $pdo)
    {
        $classMetaData = $this->getClassMetaData($object);
        $tableName = $classMetaData->getTableName();
        $properties = $classMetaData->getProperties();

        $updateData =
        $columns =
        $values = [];

        $primaryKeyColumnName =
        $primaryKeyColumnValue = '';

        foreach($properties as $columnName => $columnData){
            //TODO: implement with public getters or reflection?
            //way 1: with public getters
            $getterFunc = 'get'. ucfirst($columnName);

            if (method_exists($object, $getterFunc) && !empty($object->$getterFunc())){
                $columns[] = $columnName;
                if ($columnData['type'] != 'integer'){
                    $values[] = "'" . $object->$getterFunc() . "'";
                } else {
                    $values[] = $object->$getterFunc();
                }

                if (isset($columnData['option']) && $columnData['option'] == 'primary key'){
                    $primaryKeyColumnName = $columnName;
                    $primaryKeyColumnValue = $values[count($values)-1];
                }

                $updateData[] = $columnName . ' = ' . $values[count($values)-1];
            }
        }

        $updateData = implode(', ', $updateData);

        $sql = sprintf(
            'UPDATE "%s" SET %s WHERE %s = %s', $tableName, $updateData, $primaryKeyColumnName, $primaryKeyColumnValue
        );

        $sth = $pdo->prepare($sql);

        if (!$sth->execute()){
            throw new PersistenceException('Cannot update model in database: ' . $pdo->errorInfo());
        }
    }

    /**
     * Executes all scheduled entity deletes
     *
     * @param object $object The entity instance
     * @param \PDO $pdo
     *
     * @throws PersistenceException
     */
    public function executeDelete($object, $pdo)
    {
        $classMetaData = $this->getClassMetaData($object);
        $tableName = $classMetaData->getTableName();
        $properties = $classMetaData->getProperties();

        $primaryKeyColumnName = '';
        $primaryKeyValue = null;

        foreach($properties as $columnName => $columnData){
            if (isset($columnData['option']) && $columnData['option'] == 'primary key'){
                $primaryKeyColumnName = $columnName;

                $getterFunc = 'get'. ucfirst($columnName);
                if (method_exists($object, $getterFunc) && !empty($object->$getterFunc())){
                    if ($columnData['type'] != 'integer'){
                        $primaryKeyValue = "'" . $object->$getterFunc() . "'";
                    } else {
                        $primaryKeyValue = $object->$getterFunc();
                    }
                }
            }
        }

        if (empty($primaryKeyColumnName)){
            throw new PersistenceException('Entity class config primary key must be set.');
        }

        $sql = sprintf('DELETE FROM "%s" WHERE %s = %s', $tableName, $primaryKeyColumnName, $primaryKeyValue);

        $sth = $pdo->prepare($sql);

        if (!$sth->execute()){
            throw new PersistenceException('Cannot delete model from database: ' . $pdo->errorInfo()[2]);
        }

        $oid = spl_object_hash($object);
        $this->entityStates[$oid] = self::ENTITY_STATE_REMOVED;

        $this->removeFromIdentityMap($object);
    }

    /**
     * Returns ClassMetaData object which contains info about entity
     *
     * @param object $object The entity instance class name
     * @return ClassMetaData
     */
    public function getClassMetaData($object)
    {
        $entityClassName = get_class($object);
        return $this->entityManager->getClassMetaData($entityClassName);
    }

    /**
     * @param $entity
     * @throws PersistenceException
     */
    public function addToIdentityMap($entity)
    {
        $classMetaData = $this->getClassMetaData($entity);
        $pk = $classMetaData->getPrimaryKeyColumnName();

        $refObject   = new \ReflectionObject( $entity );
        $refProperty = $refObject->getProperty( $pk );
        if (! $refProperty->isPublic()){
            $refProperty->setAccessible( true );
        }
        $pkValue = $refProperty->getValue( $entity );
        if (! $refProperty->isPublic()){
            $refProperty->setAccessible( false );
        }

        $this->identityMap[$classMetaData->getEntityClassName()][$pkValue] = $entity;
    }

    /**
     * @param $entity
     * @throws PersistenceException
     */
    public function removeFromIdentityMap($entity)
    {
        $classMetaData = $this->getClassMetaData($entity);
        $pk = $classMetaData->getPrimaryKeyColumnName();

        $refObject   = new \ReflectionObject( $entity );
        $refProperty = $refObject->getProperty( $pk );
        if (! $refProperty->isPublic()){
            $refProperty->setAccessible( true );
        }
        $pkValue = $refProperty->getValue( $entity );
        if (! $refProperty->isPublic()){
            $refProperty->setAccessible( false );
        }

        unset($this->identityMap[$classMetaData->getEntityClassName()][$pkValue]);
    }


    /**
     * @param string $entityClassName
     * @param mixed $id Primary key value
     * @return object Fetched entity object
     * @throws PersistenceException
     */
    public function load($entityClassName, $id)
    {
        if (isset($this->identityMap[$entityClassName][$id])){
            return $this->identityMap[$entityClassName][$id];
        }

        $pdo = $this->getEntityManager()->getConnection()->connect();

        $classMetaData = $this->getEntityManager()->getClassMetaData($entityClassName);

        $primaryKeyColumnName = $classMetaData->getPrimaryKeyColumnName();

        $queryFactory = new QueryFactory($this->getEntityManager()->getConnection()->getPlatform());

        $select = $queryFactory->newSelect()->from($classMetaData->getTableName());
        $select->cols(['*']);
        $select->Where("$primaryKeyColumnName = :id")
            ->bindValue('id', $id);
        $select->limit(1);

        $queryStatement = $select->getStatement();

        $sth = $pdo->prepare($queryStatement);

        if (!$sth->execute($select->getBindValues())){
            throw new PersistenceException('Cannot load model from database: ' . $pdo->errorInfo());
        }

        $fetchedArray = $sth->fetch();

        if (!$fetchedArray){
            throw new PersistenceException('No such model in database.');
        }

        $ref = new \ReflectionClass($classMetaData->getEntityClassName());
        $entityObject = $ref->newInstanceWithoutConstructor();
        $refObject = new \ReflectionObject( $entityObject );

        foreach ($fetchedArray as $property => $value){
            if ($refObject->hasProperty($property)){
                $refProperty = $refObject->getProperty( $property );
                if ($refProperty->isPrivate() || $refProperty->isProtected()){
                    $refProperty->setAccessible( true );
                }

                $refProperty->setValue($entityObject, $value );

                if ($refProperty->isPrivate() || $refProperty->isProtected()){
                    $refProperty->setAccessible( false );
                }
            }
        }

        $oid = spl_object_hash($entityObject);
        $this->entityStates[$oid] = self::ENTITY_STATE_MANAGED;

        $this->addToIdentityMap($entityObject);

        return $entityObject;
    }
}
