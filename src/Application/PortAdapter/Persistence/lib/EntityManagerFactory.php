<?php
namespace Application\PortAdapter\Persistence\lib;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class EntityManagerFactory
 * @package Application\PortAdapter\Persistence\lib
 */
class EntityManagerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return EntityManager
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // Get class meta data
        $pathToClassMetaData = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'persistence-classes-meta-data.yaml';
        $classMetaDataList = $Data = \Spyc::YAMLLoad($pathToClassMetaData);

        // Get db configs
        $config = $serviceLocator->get('Config');
        $params = $config['db']['connection'];
        $connection = new Connection($params);

        $entityRepositoryFactory = new EntityRepositoryFactory();
        return new EntityManager($connection, $classMetaDataList, $entityRepositoryFactory);
    }

}
