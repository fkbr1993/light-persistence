<?php
namespace Application\PortAdapter\Persistence\PDO;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\PortAdapter\Persistence\lib\EntityManagerInterface;

/**
 * Class DomainSessionFactory
 * @package Application\PortAdapter\Persistence\Doctrine
 */
class DomainSessionFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return DomainSession
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $serviceLocator->get('application.portAdapter.persistence.lib.EntityManager');

        return new DomainSession($entityManager);
    }

}
