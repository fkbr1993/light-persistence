<?php
namespace Application\PortAdapter\Persistence\PDO;

use Application\Domain\DomainSessionInterface;
use Application\PortAdapter\Persistence\lib\EntityManagerInterface;

/**
 * Class DomainSession
 * @package Application\PortAdapter\Persistence\Doctrine
 */
class DomainSession implements DomainSessionInterface
{

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @param $entityManager
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     */
    public function flush()
    {
        $this->entityManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->entityManager->clear();
        $this->entityManager->getConnection()->close();
    }

}
