<?php
namespace Application\PortAdapter\TestObject\API\Http\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Application\Application\TestObject\TestObjectService;

/**
 * Class TestObjectController
 * @package Application\PortAdapter\TestObject\API\Http\Controller
 */
class TestObjectController extends AbstractActionController{

    /**
     * @return JsonModel
     */
    public function indexAction()
    {
        return new JsonModel([
            'success' => true,
            'data'    => [],
            'message' => 'Report TestObject service SOA',
        ]);
    }

    /**
     * @return JsonModel
     */
    public function createAction()
    {
        //request example
        # /api/testObject/create

        $request = $this->params()->fromPost();

        $request = ['name' => 'TestObject one', 'state' => 1];

        /** @var TestObjectService $testObjectService */
        $testObjectService = $this->getServiceLocator()->get('application.application.testObject.TestObjectService');


        $response = $testObjectService->create($request['name'], $request['state']);

        return new JsonModel($response->jsonSerialize());
    }

    /**
     * @return JsonModel
     */
    public function updateAction()
    {
        //request example
        # /api/testObject/update

        $request = $this->params()->fromPost();

        $request = ['id' => 5, 'data' => ['name' => 'TestObject one updated', 'state' => 2], ];

        /** @var TestObjectService $testObjectService */
        $testObjectService = $this->getServiceLocator()->get('application.application.testObject.TestObjectService');


        $response = $testObjectService->update($request['id'], $request['data']);

        return new JsonModel($response->jsonSerialize());
    }

    /**
     * @return JsonModel
     */
    public function deleteAction()
    {
        //request example
        # /api/testObject/delete

        $request = $this->params()->fromPost();

        $request = ['id' => 4];

        $testObjectId = $request['id'];

        /** @var TestObjectService $testObjectService */
        $testObjectService = $this->getServiceLocator()->get('application.application.testObject.TestObjectService');


        $response = $testObjectService->delete($testObjectId);

        return new JsonModel($response->jsonSerialize());
    }
}
