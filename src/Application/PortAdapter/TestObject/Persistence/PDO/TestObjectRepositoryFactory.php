<?php
namespace Application\PortAdapter\TestObject\Persistence\PDO;

use Application\Domain\PersistModel\TestObject\TestObject;
use Application\PortAdapter\Persistence\lib\EntityManagerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\PortAdapter\Persistence\lib\EntityRepositoryInterface;

/**
 * Class TestObjectRepositoryFactory
 * @package Application\PortAdapter\TestObject\Persistence\PDO
 */
class TestObjectRepositoryFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     * @throws \Exception
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $serviceLocator->get('application.portAdapter.persistence.lib.EntityManager');

        /** @var EntityRepositoryInterface $entityRepository */
        $entityRepository = $entityManager->getRepository(TestObject::class);

        return new TestObjectRepository($entityRepository, $entityManager);
    }
}
