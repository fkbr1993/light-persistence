<?php
namespace Application\PortAdapter\TestObject\Persistence\PDO;

use Application\Domain\PersistModel\TestObject\TestObject;
use Application\Domain\PersistModel\TestObject\TestObjectRepositoryInterface;
use Application\PortAdapter\Persistence\lib\EntityRepositoryInterface;
use Application\PortAdapter\Persistence\lib\EntityManagerInterface;


/**
 * Class TestObjectRepository
 *
 * @package Application\PortAdapter\TestObject\Persistence\PDO
 */
class TestObjectRepository implements TestObjectRepositoryInterface
{
    /**
     * @var EntityRepositoryInterface
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityRepositoryInterface $repository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EntityRepositoryInterface $repository,
        EntityManagerInterface $entityManager
    )
    {
        $this->repository    = $repository;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     */
    public function save(TestObject $testObject)
    {
        $this->entityManager->persist($testObject);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(TestObject $testObject)
    {
        $this->entityManager->delete($testObject);
    }

    /**
     * {@inheritDoc}
     */
    public function find($identity)
    {
        return $this->repository->find($identity);
    }
}
