<?php
namespace Application\Domain;

/**
 * Interface DomainSessionInterface
 * @package Application\Domain
 */
interface DomainSessionInterface
{

    /**
     * Flushes all changes that was made in domain session
     */
    public function flush();

    /**
     * Close all connections that are available in domain session
     */
    public function close();

}
