<?php
namespace Application\Domain\PersistModel\TestObject;

/**
 * Class TestObjectFactory
 *
 * @package Application\Domain\PersistModel\TestObject
 */
class TestObjectFactory
{
    /**
     * @param $name
     * @param $state
     *
     * @return TestObject
     */
    public function create($name, $state)
    {
        $entity = new TestObject($name, $state);

        return $entity;
    }
}
