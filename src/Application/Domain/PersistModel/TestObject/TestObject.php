<?php
namespace Application\Domain\PersistModel\TestObject;

/**
 * TestObject entity
 */
class TestObject
{

    /**
     * @var integer $id
     */
    protected $id;


    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var integer $id
     */
    protected $state;

    /**
     * @param $name
     * @param $state
     */
    public function __construct($name, $state)
    {
        $this->name = $name;
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param array $data
     */
    public function update(array $data)
    {
        if (!empty($data['id'])){
            $this->id = $data['id'];
        }

        if (!empty($data['name'])){
            $this->name = $data['name'];
        }

        if (!empty($data['state'])){
            $this->state = $data['state'];
        }
    }
}
