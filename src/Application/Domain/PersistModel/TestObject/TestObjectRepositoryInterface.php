<?php
namespace Application\Domain\PersistModel\TestObject;

/**
 * Interface PartnerRepositoryInterface
 * @package Application\Domain\PersistModel\TestObject
 */
interface TestObjectRepositoryInterface
{

    /**
     * Returns TestObject if it can be found by given identity, otherwise null is returned
     *
     * @param integer $identity
     * @return TestObject|null
     */
    public function find($identity);

    /**
     * Saves given TestObject to database
     * @param TestObject $testObject
     * @return mixed
     */
    public function save(TestObject $testObject);

    /**
     * Deletes given TestObject from database
     * @param TestObject $testObject
     * @return mixed
     */
    public function delete(TestObject $testObject);

    //TODO: implement search
}
